﻿using System.ComponentModel.DataAnnotations;

namespace EdisonVote.WebUI.Models.VoteViewModels
{
    /// <summary>
    /// Модель создания голосования
    /// </summary>
    public class CreateVoteViewModel
    {
        /// <summary>
        /// Пользователь может создать голосование заново
        /// </summary>
        public bool CanRecreate { get; set; }

        [Display(Name="Продолжительность первой фазы, мин")]
        public int Phase1Duration { get; set; }

        [Display(Name = "Продолжительность второй фазы, мин")]
        public int Phase2Duration { get; set; }
    }
}
