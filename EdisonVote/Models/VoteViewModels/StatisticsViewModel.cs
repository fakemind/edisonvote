﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EdisonVote.Domain.Voting;

namespace EdisonVote.WebUI.Models.VoteViewModels
{
    public class StatisticsViewModel
    {
        public List<Statistics> Winners { get; set; }

        public List<VoteResult> All { get; set; }

        /// <summary>
        /// Пользователь может проголосовать
        /// </summary>
        public bool UserCanVote { get; set; }
    }
}
