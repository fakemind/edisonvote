﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EdisonVote.Domain.Voting;

namespace EdisonVote.WebUI.Models.VoteViewModels
{
    /// <summary>
    /// Модель для голосования
    /// </summary>
    public class VoteViewModel
    {
        /// <summary>
        /// Доступные даты
        /// </summary>
        public List<string> AvailableDates { get; set; }

        /// <summary>
        /// Выбранная дата
        /// </summary>
        [Display(Name="Время")]
        public string SelectedDate { get; set; }

        /// <summary>
        /// Название мероприятия
        /// </summary>
        [Display(Name="Мероприятие")]
        [Required]
        public string EventName { get; set; }
    }

    public class Phase2ViewModel
    {
        /// <summary>
        /// Пользователь может проголосовать, т.к. его выбор не победил
        /// </summary>
        public bool UserCanVote { get; set; }

        public List<Statistics> Winners { get; set; }
    }
}
