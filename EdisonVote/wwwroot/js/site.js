﻿const apiUrl = "/edison/";


var phase1TimeLeft = 0;
var timeLeft = 0;
var voters = 0;


function formatData(votes, phase1TimeLeft, timeLeft) {
    if (phase1TimeLeft < 0) phase1TimeLeft = 0;
    const hoursPhase = Math.floor(phase1TimeLeft / 3600);
    const minutesPhase = Math.floor(phase1TimeLeft / 60 % 60);
    const secondsPhase = phase1TimeLeft % 60;

    const hours = Math.floor(timeLeft / 3600);
    const minutes = Math.floor(timeLeft / 60 % 60);
    const seconds = timeLeft % 60;

    $("#info").html(`Проголосовало: ${votes} <br />До конца 1 фазы голосования: ${hoursPhase}` +
        ` ч. ${minutesPhase} мин. ${secondsPhase} сек. <br />До конца голосования: ${hours}` +
        ` ч. ${minutes} мин. ${seconds} сек.`);
}


var timers = function () {
    setInterval(function () {
            if (phase1TimeLeft > 0) {
                phase1TimeLeft--;
                // Показываем статистику, если фаза заканчивается
                if (phase1TimeLeft == 0) {
                    location.href = apiUrl + "vote/statistics";
                }
            } else {
                phase1TimeLeft = 0;
            }
            if (timeLeft > 0) {
                timeLeft--;
            } else {
                timeLeft = 0;
            }

            formatData(voters, phase1TimeLeft, timeLeft);
        },
        1000);
}

function refuse() {
    $("#winners").html("<img src=\"/images/loading.gif\" width=\"100\"/>");
    $.getJSON(apiUrl + "vote/refuseEvent", function (data) {
    });
}

function accept(nameId, time) {
    $("#winners").html("<img src=\"/images/loading.gif\" width=\"100\"/>");
    $.getJSON(apiUrl + "vote/acceptEvent", { nameId, time }, function (data) {
    });
}


$(function () {

    // Signal R
    let hubUrl = apiUrl + "notifications";
    let httpConnection = new signalR.HttpConnection(hubUrl);
    let hubConnection = new signalR.HubConnection(httpConnection);

    hubConnection.on("Send",
        function (data) {
            voters = data.voters;
            timeLeft = data.timeLeft;
            phase1TimeLeft = data.phase1TimeLeft;
            formatData(voters, phase1TimeLeft, timeLeft);
        });

    hubConnection.on("Chart",
        function(data) {
            chart(data.innerData, data.outerData);

            // Обновляем таблицу с победителями
            $.get(apiUrl + "vote/winners", function (data) {
                $("#winners").html(data);
            });
        });

    hubConnection.start();

    let $chart = $("#chart");
    let $info = $("#info");
    let $winners = $("#winners");
    let $eventName = $("#EventName");
    
    if ($chart.length > 0) {
        $.getJSON(apiUrl + "vote/chart",
            function(data) {
                chart(data.innerData, data.outerData);
            });
    }

    if ($info.length > 0) {
        $.getJSON(apiUrl + "vote/timeLeft",
            function(data) {
                // Таймеры
                voters = data.voters;
                timeLeft = data.timeLeft;
                phase1TimeLeft = data.phase1TimeLeft;
                formatData(voters, phase1TimeLeft, timeLeft);
                timers(voters, timeLeft, phase1TimeLeft);
            });
    }

    if ($winners.length > 0) {
        $.get(apiUrl + "vote/winners",
            function(data) {
                $winners.html(data);
            });
    }

    if ($eventName.length > 0) {
        $eventName.autocomplete(
            {
                serviceUrl: apiUrl + 'vote/variants',
                paramName: 'inputStr',
                minChars: 2,
                dataType: 'json',
                transformResult: function(response) {
                    return {
                        suggestions: $.map(response,
                            function(dataItem) {
                                return { value: dataItem, data: dataItem };
                            })
                    };
                }

            }
        );
    }
});