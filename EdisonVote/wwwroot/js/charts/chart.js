﻿
var chart = function (innerData, outerData) {

    console.log(outerData);
    var dom = document.getElementById("chart");
    var myChart = echarts.init(dom);
    var app = {};
    var option = null;
    app.title = 'Голосование';

    option = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },

        series: [
            {
                name: 'Время',
                type: 'pie',
                selectedMode: 'single',
                radius: [0, '30%'],

                label: {
                    normal: {
                        position: 'inner'
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: innerData
            },
            {
                name: 'Мероприятие',
                type: 'pie',
                radius: ['40%', '55%'],
                tooltip: {
                    formatter: function (params) {                        
                        return "Проголосовали:<br />" + params.data.voters.join('<br />');
                    },
                   
                },
                label: {
                    normal: {
                        formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c} {per|{d}%}  ',
                        //formatter: function(params) {
                        //    console.log(params);
                        //    return `${params.name}\n{hr|}\n ${params.value} (${params.percent}%)\n{hr|}\n${params.data.voters.join(",")}`;
                            
                        //},
                        backgroundColor: '#eee',
                        borderColor: '#aaa',
                        borderWidth: 1,
                        borderRadius: 4,
                         shadowBlur:3,
                         shadowOffsetX: 2,
                         shadowOffsetY: 2,
                         shadowColor: '#999',
                         padding: [0, 7],
                        rich: {
                            a: {
                                color: '#999',
                                lineHeight: 22,
                                align: 'center'
                            },
                            // abg: {
                            //     backgroundColor: '#333',
                            //     width: '100%',
                            //     align: 'right',
                            //     height: 22,
                            //     borderRadius: [4, 4, 0, 0]
                            // },
                            hr: {
                                borderColor: '#aaa',
                                width: '100%',
                                borderWidth: 0.5,
                                height: 0
                            },
                            b: {
                                fontSize: 16,
                                lineHeight: 33
                            },
                            per: {
                                color: '#eee',
                                backgroundColor: '#334455',
                                padding: [2, 4],
                                borderRadius: 2
                            }
                        }
                    }
                },
                data: outerData
            }
        ]
    };;
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
}