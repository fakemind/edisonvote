﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EdisonVote.Domain.Authentication;
using EdisonVote.Domain.Services;
using EdisonVote.WebUI.Hubs;
using EdisonVote.WebUI.Models.VoteViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace EdisonVote.WebUI.Controllers
{
    [Authorize]
    public class VoteController : Controller
    {
        /// <summary>
        /// Час окончания
        /// </summary>
        private int _endHour = 23;

        /// <summary>
        /// Шаг в минутах
        /// </summary>
        private int _stepMinutes = 15;

        private readonly VoteService _voteService;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly IHubContext<VoteHub> _hubContext;

        public VoteController(VoteService voteService, UserManager<ApplicationUser> userManager,IHubContext<VoteHub> hubContext)
        {
            _voteService = voteService;
            _userManager = userManager;
            _hubContext = hubContext;
        }

        /// <summary>
        /// Форма голосования
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var activeVote = await _voteService.GetActiveVote();
            if (activeVote == null)
            {
                ModelState.AddModelError("", "В данный момент нет активного голосования");
                return Redirect(Url.Action("Create"));
            }

            // если первая часть голосования
            if (activeVote.TimeLeftUntilPhase1Finish().TotalMinutes > 0)
            {
                return View(new VoteViewModel()
                {
                    AvailableDates = FIllDates()
                });
            }
            // если вторая часть голосования
            return Redirect(Url.Action("Statistics"));
        }


        [HttpPost]
        [ActionName("Index")]
        public async Task<IActionResult> Store(VoteViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var date = ParseTime(model.SelectedDate);
                    if (date == null)
                    {
                        ModelState.AddModelError("", "Неверное значение времени");
                    }

                    var userId = _userManager.GetUserId(User);
                    var resultId = await _voteService.StoreResult(model.EventName, new DateTimeOffset(date.Value), 1, userId);

                    // Уведомляем участников об изменении голосов
                    await NotifyClients();
                    ViewBag.Success = true;
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            model.AvailableDates = FIllDates();
            return View("Index", model);
        }

        /// <summary>
        /// Приведение поступившей даты в нужный формат
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private DateTime? ParseTime(string date)
        {
            if (string.IsNullOrEmpty(date)) return null;
            var dateParts = date.Split(":");
            if (dateParts.Length != 2)
            {
                return null;
            }
         
            var hours = Int32.Parse(dateParts[0]);
            var minutes = Int32.Parse(dateParts[1]);
            var dateValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hours, minutes, 00);
            return dateValue;
        }

        /// <summary>
        /// Уведомление клиентов об изменении количества голосов
        /// </summary>
        /// <returns></returns>
        private async Task NotifyClients()
        {
            var activeVote = await _voteService.GetActiveVote();

            var voters = await _voteService.CountVoters(activeVote.Id);
            var phase1TimeLeft = (int) activeVote.TimeLeftUntilPhase1Finish().TotalSeconds;
            var timeLeft = (int) activeVote.TimeLeftUntiFinish().TotalSeconds;
            await _hubContext.Clients.All.InvokeAsync("send", new { voters, phase1TimeLeft, timeLeft });
        }

        /// <summary>
        /// Отправка новых данных диаграммы
        /// </summary>
        /// <returns></returns>
        private async Task SendChartData()
        {
            var data = await GetChartData();
            if (data != null)
            {
                await _hubContext.Clients.All.InvokeAsync("chart", new { innerData = data.Item1, outerData = data.Item2 });
            }
        }

        /// <summary>
        /// Заполнение доступных дат
        /// </summary>
        /// <returns></returns>
        private List<string> FIllDates()
        {
            var availableDates = new List<string>();
            var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 00, 00);
            var finishDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, _endHour, 00, 00);
            if (finishDate < startDate)
            {
                finishDate = finishDate.AddDays(1);
            }

            while (startDate <= finishDate)
            {
                availableDates.Add(startDate.ToShortTimeString());
                startDate = startDate.AddMinutes(_stepMinutes);
            }
            return availableDates;
        }

        /// <summary>
        /// Автодополнение
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        public async Task<IActionResult> Variants(string inputStr)
        {
            return Ok(await _voteService.Autocomplete(inputStr));
        }

        /// <summary>
        /// Результаты первой фазы голосования
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Statistics()
        {
            var activeVote = await _voteService.GetActiveVote();
            // Нет голосования
            if (activeVote == null)
            {
                return Redirect("Create");
            }
            // Идет голосование в первой фазе
            if (activeVote.TimeLeftUntilPhase1Finish() > TimeSpan.Zero)
            {
                return Redirect("Index");
            }

            // если первая часть голосования
            return View();
        }

        /// <summary>
        /// Список доступных дат и событий для диаграммы
        /// </summary>
        /// <returns></returns>
        private async Task<Tuple<List<object>, List<object>>> GetChartData()
        {
            var activeVote = await _voteService.GetActiveVote();
            if (activeVote == null)
            {
                return null;
            }
            var data = (await _voteService.VoteResults(activeVote.Id)).OrderBy(x => x.EventDate).ToList();
            var selectedTimes = data.Select(x => x.EventDate).Distinct().OrderBy(x => x.DateTime).ToList();
            var outerData = new List<object>();
            var innerData = new List<object>();

            foreach (var time in selectedTimes)
            {
                var events = data.Where(x => x.EventDate.Equals(time)).ToList();
                var count = events.Count;
                innerData.Add(new { value = count, name = time.ToLocalTime().ToString("HH:mm") });
                
            }
            foreach (var ev in data)
            {
                var voters = data.Where(x => x.NameId == ev.NameId && x.EventDate == ev.EventDate).ToList();
                outerData.Add(new { value = voters.Count, name = ev.Name.Name, voters = voters.Select(x => x.User.Name).ToList() });
            }
            return new Tuple<List<object>, List<object>>(innerData, outerData);
        }


        /// <summary>
        /// Данные для диаграммы
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Chart()
        {
            var data = await GetChartData();
            if (data == null)
            {
                return NoContent();
            }
            return Ok(new {innerData = data.Item1, outerData = data.Item2});
        }

        /// <summary>
        /// Список победивших голосований
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Winners()
        {
            var activeVote = await _voteService.GetActiveVote();
            if (activeVote == null)
            {
                ModelState.AddModelError("", "В данный момент нет активного голосования");
                return PartialView(new StatisticsViewModel());
            }
            var winners = await _voteService.Winners(activeVote.Id);
            var userId = _userManager.GetUserId(User);
            var userCanVote = await _voteService.UserCanVote(userId, activeVote.Id, winners);

            return PartialView(new StatisticsViewModel()
            {
                Winners = winners,
                UserCanVote = userCanVote
            });
        }

        /// <summary>
        /// Оставшеемя время по текущему голосованию
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> TimeLeft()
        {
            var activeVote = await _voteService.GetActiveVote();
            var voters = await _voteService.CountVoters(activeVote.Id);

            var phase1TimeLeft = activeVote.TimeLeftUntilPhase1Finish();
            var timeLeft = activeVote.TimeLeftUntiFinish();

            return Ok(new
            {
                voters,
                phase1TimeLeft = (int)phase1TimeLeft.TotalSeconds,
                timeLeft = (int)timeLeft.TotalSeconds
            });
        }



        /// <summary>
        /// Форма создания голосования
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Create")]
        public async Task<IActionResult> Create()
        {
            var userId = _userManager.GetUserId(User);
            var model = new CreateVoteViewModel();

            var activeVote = await _voteService.GetActiveVote();
            if (activeVote != null)
            {
                ModelState.AddModelError("", "В данный момент активно другое голосование");
                model.CanRecreate = (userId == activeVote.CreatorId);
            }
            return View(model);
        }

        /// <summary>
        /// Сохранение формы голосования
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Create")]
        public async Task<IActionResult> CreateHandler(CreateVoteViewModel model)
        {
            if (model.Phase1Duration <= 0)
            {
                ModelState.AddModelError(nameof(model.Phase1Duration), "Продолжительность фазы не может быть меньше 1 минуты");
            }
            if (model.Phase2Duration <= 0)
            {
                ModelState.AddModelError(nameof(model.Phase2Duration), "Продолжительность фазы не может быть меньше 1 минуты");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userId = _userManager.GetUserId(User);
                    var voteId = await _voteService.StoreVote(model.Phase1Duration, model.Phase2Duration, userId);
                    TempData["Created"] = voteId;
                    return Redirect(Url.Action("Index"));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View("Create", model);
                }
            }
            else
            {
                return View("Create", model);
            }
        }

        /// <summary>
        /// Согласиться с мероприятием
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> AcceptEvent(int nameId, string time)
        {
            try
            {
                var dateTime = ParseTime(time);
                if (dateTime == null)
                {
                    return BadRequest("Неверный формат времени");
                }

                var userId = _userManager.GetUserId(User);
                var voteId = await _voteService.AcceptEvent(nameId, new DateTimeOffset(dateTime.Value), userId);
                await SendChartData();
                await NotifyClients();

                return Ok(voteId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Отказаться от мероприятий
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> RefuseEvent()
        {
            try
            {
                var userId = _userManager.GetUserId(User);
                var voteId = await _voteService.RefuseEvent(userId);
                await SendChartData();
                await NotifyClients();
                return Ok(voteId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
         
        }
    }
}