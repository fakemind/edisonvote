﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace EdisonVote.WebUI.Hubs
{
    public class VoteHub : Hub
    {
        /// <summary>
        /// Новые данные статистики
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task Send(string message)
        {
            await Clients.All.InvokeAsync("Send", message);
        }

        /// <summary>
        /// Отправляет новые данные для диаграммы
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendStatistics(string message)
        {
            await Clients.All.InvokeAsync("Statistics", message);
        }
    }
}
