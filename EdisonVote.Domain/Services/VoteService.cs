﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EdisonVote.Domain.Authentication;
using EdisonVote.Domain.Storage;
using EdisonVote.Domain.Voting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace EdisonVote.Domain.Services
{
    /// <summary>
    /// Сервис обработки голосования
    /// </summary>
    public class VoteService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public VoteService(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        /// <summary>
        /// Получение активного голосования
        /// </summary>
        /// <returns></returns>
        public Task<Vote> GetActiveVote()
        {
            return _context
                .Votes
                .Where(x => x.DateStart <= DateTimeOffset.UtcNow && x.DateFinish > DateTimeOffset.UtcNow)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Сохранение голосования
        /// </summary>
        /// <param name="phase1Duration"></param>
        /// <param name="phase2Duration"></param>
        /// <param name="creatorId"></param>
        /// <returns></returns>
        public async Task<int> StoreVote(int phase1Duration, int phase2Duration, string creatorId)
        {
            var activeVote = await GetActiveVote();
            if (activeVote != null)
            {
                if (activeVote.CreatorId != creatorId) // Если это не пересоздание голосования
                {
                    throw new Exception("Существует активное голосование");
                }
                else // Удаляем текущее голосование
                {
                    _context.Votes.Remove(activeVote);
                    await _context.SaveChangesAsync();
                }
            }

            var vote = new Vote()
            {
                CreatorId = creatorId,
                DateStart = DateTimeOffset.UtcNow,
                DateFinish = DateTimeOffset.UtcNow.AddMinutes(phase1Duration + phase2Duration),
                Phase1Duration = phase1Duration
            };
            await _context.Votes.AddAsync(vote);
            await _context.SaveChangesAsync();
            return vote.Id;
        }

        /// <summary>
        /// Сохранить, что человек проголосовал
        /// </summary>
        /// <returns></returns>
        public async Task<int> StoreResult(string name, DateTimeOffset eventDate, int phaseNumber, string userId)
        {
            var activeVote = await GetActiveVote();
            if (activeVote == null)
            {
                throw new Exception("Не существует активного голосования");
            }

            if (await IsVoted(activeVote.Id, userId))
            {
                throw new Exception("Вы уже голосовали");
            }

            var nameId = await GetNameId(name);
            var result = new VoteResult()
            {
                EventDate = eventDate.ToUniversalTime(),
                NameId = nameId,
                UserId = userId,
                VoteId = activeVote.Id,
                PhaseNumber = phaseNumber
            };

            await _context.VoteResults.AddAsync(result);
            await _context.SaveChangesAsync();
            return result.Id;
        }

        /// <summary>
        /// Получает результат голосования пользователя
        /// </summary>
        /// <returns></returns>
        public Task<List<VoteResult>> GetUserVotes(int voteId, string userId)
        {
            return _context
                .VoteResults
                .Where(x => x.UserId == userId && x.VoteId == voteId)
                .ToListAsync();
        }

        /// <summary>
        /// Проверяет, может ли пользователь голосовать во второй фазе
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="voteId"></param>
        /// <param name="winners"></param>
        /// <returns></returns>
        /// <remarks>Пользователь может голосовать, если его голос был отдал не за победителя и во второй фазе он не голосовал</remarks>
        public async Task<bool> UserCanVote(string userId, int voteId, List<Statistics> winners)
        {
            var userVotes = await GetUserVotes(voteId, userId);
            if (userVotes.Exists(x => x.PhaseNumber == 2)) // если он уже голосовал во второй фазе - то нет
            {
                return false;
            }
            var phase1Vote = userVotes.FirstOrDefault(x => x.PhaseNumber == 1);
            if (phase1Vote == null) // если в первой фазе он не голосовал - то тоже может
            {
                return true;
            }
            // иначе если голосовал не за победителя
            return (winners.Exists(x => x.Date == phase1Vote.EventDate && x.Name.Id == phase1Vote.NameId) == false);

        }

        /// <summary>
        /// Проверяем, что человек уже голосовал
        /// </summary>
        /// <param name="voteId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<bool> IsVoted(int voteId, string userId)
        {
            return (await _context
                       .VoteResults
                       .Where(x => x.VoteId == voteId && x.UserId == userId)
                       .CountAsync()) != 0;
        }

        /// <summary>
        /// Получение номера голосования
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private async Task<int> GetNameId(string name)
        {
            var nameId = await _context.Names.Where(x => x.Name == name).Select(x => x.Id).FirstOrDefaultAsync();
            if (nameId == 0)
            {
                var eventName = new EventName()
                {
                    Name = name
                };
                await _context.Names.AddAsync(eventName);
                await _context.SaveChangesAsync();
                nameId = eventName.Id;
            }
            return nameId;
        }
        /// <summary>
        /// Варианты для автодополнения
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        public async Task<List<string>> Autocomplete(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr) || inputStr.Length < 2) return null;

            var testStr = inputStr.ToLower();
            return await _context
                .Names
                .Where(x => x.Name.ToLower().StartsWith(testStr))
                .OrderBy(x => x.Name)
                .Select(x => x.Name)
                .ToListAsync();
        }

        /// <summary>
        /// Количество проголосовавших
        /// </summary>
        /// <param name="voteId"></param>
        /// <returns></returns>
        public Task<int> CountVoters(int voteId)
        {
            return _context
                .VoteResults
                .Where(x => x.VoteId == voteId)
                .CountAsync();
        }

        /// <summary>
        /// Победители голосования
        /// </summary>
        /// <param name="voteId"></param>
        /// <returns></returns>
        public async Task<List<Statistics>> Winners(int voteId)
        {
            var result = await _context.VoteResults
                .Where(x => x.VoteId == voteId && x.PhaseNumber == 1)
                .GroupBy(x => new {x.NameId, x.EventDate})
                .Select(x => new
                {
                    Name = x.Key,
                    Count = x.Count()
                })
                .OrderByDescending(x => x.Count)
                .ToListAsync();

            var topResult = result.FirstOrDefault();
            if (topResult == null)
            {
                return null;
            }

            var topResults = result.Where(x => x.Count == topResult.Count).ToList();
            var topResultIds = topResults.Select(x => x.Name.NameId).ToList();
            var names = await _context.Names.Where(x => topResultIds.Contains(x.Id)).ToListAsync();
            var winners = new List<Statistics>();
            
            foreach (var top in topResults)
            {
                var userIds = await _context
                    .VoteResults
                    .Where(x => x.EventDate == top.Name.EventDate && x.NameId == top.Name.NameId)
                    .Select(x => x.UserId)
                    .ToListAsync();

                var users = await _userManager.Users.Where(x => userIds.Contains(x.Id)).ToListAsync();

                winners.Add(new Statistics()
                {
                    Date = top.Name.EventDate,
                    Name = names.FirstOrDefault(x => x.Id == top.Name.NameId),
                    Users = users
                });
            }

            return winners;
        }

        /// <summary>
        /// Результаты голосования
        /// </summary>
        /// <param name="voteId"></param>
        /// <returns></returns>
        public Task<List<VoteResult>> VoteResults(int voteId)
        {
            var results = _context.VoteResults
                .Include(x => x.Name)
                .Include(x => x.User)
                .Where(x => x.VoteId == voteId && x.EventDate != DateTimeOffset.MinValue)
                .ToListAsync();
            return results;
        }

        /// <summary>
        /// Пользователь принимает мероприятие
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="date"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<int> AcceptEvent(int nameId, DateTimeOffset date, string userId)
        {
            var activeVote = await GetActiveVote();
            if (activeVote == null)
            {
                throw new Exception("Не существует активного голосования");
            }

            var userVotes = await GetUserVotes(activeVote.Id, userId);
            if (userVotes.Exists(x => x.PhaseNumber == 2)) // Пользователь уже выбирал
            {
                throw new Exception("Вы уже сделали выбор");
            }

            if (userVotes.Exists(x => x.NameId == nameId && x.EventDate == date))
            {
                throw new Exception("Вы уже выбрали этот вариант");
            }
           
            var result = new VoteResult()
            {
                EventDate = date.ToUniversalTime(),
                NameId = nameId,
                UserId = userId,
                VoteId = activeVote.Id,
                PhaseNumber = 2
            };
            await _context.VoteResults.AddAsync(result);
            await _context.SaveChangesAsync();
            return result.Id;
        }

        /// <summary>
        /// Пользователь отказывается от мероприятий
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<int> RefuseEvent(string userId)
        {
            var activeVote = await GetActiveVote();
            if (activeVote == null)
            {
                throw new Exception("Не существует активного голосования");
            }

            var userVotes = await GetUserVotes(activeVote.Id, userId);
            if (userVotes.Exists(x => x.PhaseNumber == 2)) // Пользователь уже выбирал
            {
                throw new Exception("Вы уже сделали выбор");
            }

            var anyName = await _context.Names.FirstOrDefaultAsync();

            // Показатель отсутствия выбора - дата с минимальным значением
            var result = new VoteResult()
            {
                EventDate = DateTimeOffset.MinValue,
                NameId = anyName.Id,
                UserId = userId,
                VoteId = activeVote.Id,
                PhaseNumber = 2
            };
            await _context.VoteResults.AddAsync(result);
            await _context.SaveChangesAsync();
            return result.Id;
        }
    }
}
