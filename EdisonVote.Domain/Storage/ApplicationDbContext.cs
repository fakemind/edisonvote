﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EdisonVote.Domain.Authentication;
using EdisonVote.Domain.Voting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace EdisonVote.Domain.Storage
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Список голосований
        /// </summary>
        public DbSet<Vote> Votes { get; set; }

        public DbSet<VoteResult> VoteResults { get; set; }

        public DbSet<EventName> Names { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Vote>()
                .HasKey(x => x.Id);

            builder.Entity<Vote>()
                .HasMany(x => x.Results)
                .WithOne(x => x.Vote)
                .HasForeignKey(x => x.VoteId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Vote>()
                .HasOne(x => x.Creator)
                .WithMany(x => x.Votes)
                .HasForeignKey(x => x.CreatorId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<VoteResult>()
                .HasKey(x => x.Id);

            builder.Entity<VoteResult>()
                .HasOne(x => x.User)
                .WithMany(x => x.VoteResults)
                .HasForeignKey(x => x.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);


            builder.Entity<VoteResult>()
                .HasOne(x => x.Name)
                .WithMany()
                .HasForeignKey(x => x.NameId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<EventName>()
                .HasKey(x => x.Id);

        }


    }
}
