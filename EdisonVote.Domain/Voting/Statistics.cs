﻿using System;
using System.Collections.Generic;
using System.Text;
using EdisonVote.Domain.Authentication;

namespace EdisonVote.Domain.Voting
{
    /// <summary>
    /// Результаты голосования по пункту
    /// </summary>
    public class Statistics
    {
        public EventName Name { get; set; }
        public DateTimeOffset Date { get; set; }
        public List<ApplicationUser> Users { get; set; }
    }
}
