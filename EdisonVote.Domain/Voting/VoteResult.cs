﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using EdisonVote.Domain.Authentication;

namespace EdisonVote.Domain.Voting
{
    public class VoteResult
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Номер голосования
        /// </summary>
        public int VoteId { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        public EventName Name { get; set; }

        public int NameId { get; set; }

        public DateTimeOffset EventDate { get; set; }

        public Vote Vote { get; set; }

        /// <summary>
        /// Номер фазы, когда голосовал пользователь
        /// </summary>
        public int PhaseNumber { get; set; }
    }
}
