﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using EdisonVote.Domain.Authentication;

namespace EdisonVote.Domain.Voting
{
    /// <summary>
    /// Голосование
    /// </summary>
    public class Vote
    {
        public Vote()
        {
            Results = new List<VoteResult>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTimeOffset DateStart { get; set; }

        public int Phase1Duration { get; set; }

        public DateTimeOffset DateFinish { get; set; }

        public string CreatorId { get; set; }

        public ICollection<VoteResult> Results { get; set; }

        public ApplicationUser Creator { get; set; }

        /// <summary>
        /// Время до окончания первой фазы
        /// </summary>
        /// <returns></returns>
        public TimeSpan TimeLeftUntilPhase1Finish()
        {
            return (DateStart.AddMinutes(Phase1Duration)) - DateTimeOffset.UtcNow;
        }

        /// <summary>
        /// Врем до конца голосования
        /// </summary>
        /// <returns></returns>
        public TimeSpan TimeLeftUntiFinish()
        {
            return DateFinish - DateTimeOffset.UtcNow;
        }
    }
}
