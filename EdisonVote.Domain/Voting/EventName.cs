﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EdisonVote.Domain.Voting
{
    /// <summary>
    /// Название мероприятия, для словаря
    /// </summary>
    public class EventName
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
