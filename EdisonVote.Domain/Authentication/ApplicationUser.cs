﻿using System.Collections.Generic;
using EdisonVote.Domain.Voting;
using Microsoft.AspNetCore.Identity;

namespace EdisonVote.Domain.Authentication
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Имя пользователя 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Голосования, которые создал пользователь
        /// </summary>
        public ICollection<Vote> Votes { get; set; }

        /// <summary>
        /// Все, за что проголосовал пользователь
        /// </summary>
        public ICollection<VoteResult> VoteResults { get; set; }
    }
}
